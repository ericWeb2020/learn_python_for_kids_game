# Import pygame and other modules
import os
import pygame
from src.MapElement import MapElement


# Define a subclass for the block map element
class RegularBlock(MapElement):
    # Override the constructor
    def __init__(self):
        # Call the parent class constructor with the block image
        MapElement.__init__(self, pygame.image.load(os.path.join('../img', 'regularblock.png')).convert(), "rb")