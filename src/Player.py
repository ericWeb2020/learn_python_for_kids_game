# Import pygame and other modules
import pygame
import os

# Define constants for the screen width and height
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600


# Define a class for the player
class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.images = []
        for i in range(1, 5):
            img = pygame.image.load(os.path.join('../img', 'hero' + str(i) + '.png')).convert()
            img = pygame.transform.scale(img, (32, 32))
            self.images.append(img)
            self.image = self.images[0]
            self.rect = self.image.get_rect()

        # Set the initial position and speed
        self.rect.x = 0
        self.rect.y = 32
        self.speed_x = 5
        self.speed_y = 0

        # Set the animation frame and counter
        self.frame = 0
        self.counter = 0

    # Update the player
    def update(self):
        # Move the player horizontally
        self.rect.x += self.speed_x

        # Check if the player reaches the screen edges
        if self.rect.x < 0:
            self.rect.x = 0
        elif self.rect.x > SCREEN_WIDTH - self.rect.width:
            self.rect.x = SCREEN_WIDTH - self.rect.width

        # Move the player vertically
        self.rect.y += self.speed_y

        # Check if the player reaches the screen edges
        if self.rect.y < 0:
            self.rect.y = 0
        elif self.rect.y > SCREEN_HEIGHT - self.rect.height:
            self.rect.y = SCREEN_HEIGHT - self.rect.height

        # Animate the player
        self.counter += 1
        if self.counter == 10:
            # Reset the counter
            self.counter = 0
            # Update the frame
            self.frame = (self.frame + 1) % len(self.images)
            # Update the image
            self.image = self.images[self.frame]

    def handle_user_input(self, user_input):
        # Check if the user input contains "player.advance"
        if "player.advance" in user_input:
            self.advance()

        if "player.down" in user_input:
            self.down()

    def advance(self):
        # Move the player by one cell (32 pixels) in the x-direction
        self.rect.x += 32

    def down(self):
        # Move the player by one cell (32 pixels) in the y-direction
        self.rect.y += 32
