import os
import pygame
from src.MapFactory import MapFactory
from Player import Player
from TextEditor import TextEditor

# Define screen size and fps as constants
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
FPS = 60

# Define colors as constants
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)


# This is the main Game class
class Game:
    # The constructor initializes the game
    def __init__(self):
        pygame.init()  # Initialize pygame
        self.screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))  # Set the display mode
        pygame.display.set_caption("Learn Python!")  # Set the window caption
        self.tile_size = 32  # Set the tile size
        current_dir = os.path.dirname(os.path.abspath(__file__))  # Get the current directory
        map_path = os.path.join(current_dir, 'map.txt')  # Get the path to the map file
        self.map_factory = MapFactory(map_path)  # Create a MapFactory instance
        self.map_sprites = self.map_factory.generate_map()  # Generate the map
        self.player = Player()  # Create a Player instance
        self.player_group = pygame.sprite.Group()  # Create a sprite group for the player
        self.player_group.add(self.player)  # Add the player to the sprite group

        # Create a TextEditor instance (after initializing self.screen)
        self.text_editor = TextEditor(self.screen)

        self.running = True  # Set the running state to True

    # This method runs the game
    def run(self):
        while self.running:  # While the game is running
            # Get user input from the TextEditor
            user_input = self.text_editor.text

            self.handle_events(self.player, user_input)  # Handle events
            self.update()  # Update the game state
            self.draw()  # Draw the game state
            pygame.display.flip()  # Update the display

    # This method handles events
    def handle_events(self, player_instance, user_input):
        self.text_editor.handle_events()  # Handle events in the text editor
        player_instance.handle_user_input(user_input)  # Handle user input in the player instance

        # Check if the user input contains "player.advance"
        if "player.advance" in user_input:
            # Clear the text input after processing "player.advance"
            self.text_editor.text = ""

        if "player.down" in user_input:
            self.text_editor.text = ""

        for event in pygame.event.get():  # For each event
            if event.type == pygame.QUIT:  # If the event is QUIT
                self.running = False  # Set the running state to False

    # This method updates the game state
    def update(self):
        dt = pygame.time.Clock().tick(FPS)  # Get the time delta
        self.text_editor.update(dt)  # Update the text editor

    # This method draws the game state
    def draw(self):
        self.screen.fill(BLACK)  # Fill the screen with black
        self.map_sprites.draw(self.screen)  # Draw the map sprites
        self.player_group.draw(self.screen)  # Draw the player group

        # Draw the text editor
        self.text_editor.draw()

        pygame.display.flip()  # Update the display


# If this script is run directly, create a Game instance and run it
if __name__ == "__main__":
    game = Game()
    game.run()
    pygame.quit()  # Quit pygame when the game is done
