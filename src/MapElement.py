# Import pygame and other modules
import pygame


# Define a class for the map elements
class MapElement(pygame.sprite.Sprite):
    tile_tag = ""

    # Initialize the map element
    def __init__(self, image, tile):
        # Call the parent class constructor
        pygame.sprite.Sprite.__init__(self)

        # Set the image and rectangle
        self.image = pygame.transform.scale(image, (32, 32))
        self.rect = self.image.get_rect()
        self.tile_tag = tile

    # Set the position of the map element
    def set_position(self, x, y):
        self.rect.x = x
        self.rect.y = y
