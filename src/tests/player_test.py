import unittest
from src.Player import Player


class TestPlayer(unittest.TestCase):
    def setUp(self):
        self.player = Player()

    def test_initial_position(self):
        # Test initial position of the player
        self.assertEqual(self.player.rect.x, 0)
        self.assertEqual(self.player.rect.y, 32)

    def test_movement(self):
        # Test movement of the player
        self.player.advance()
        self.assertEqual(self.player.rect.x, 32)  # Player moves 32 pixels to the right
        self.assertEqual(self.player.rect.y, 32)  # Player should stay at the same y-coordinate

        self.player.down()
        self.assertEqual(self.player.rect.x, 32)  # Player should stay at the same x-coordinate
        self.assertEqual(self.player.rect.y, 64)  # Player moves 32 pixels down

    def test_speed(self):
        # Test speed of the player
        self.assertEqual(self.player.speed_x, 5)  # Check initial speed in the x-direction
        self.assertEqual(self.player.speed_y, 0)  # Check initial speed in the y-direction


if __name__ == '__main__':
    unittest.main()
