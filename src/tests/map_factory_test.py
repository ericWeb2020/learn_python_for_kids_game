import unittest
import pygame
from src.DeadlyBlock import DeadlyBlock
from src.MapFactory import MapFactory


class TestMapFactory(unittest.TestCase):
    def setUp(self):
        # Initialize pygame
        pygame.init()
        self.screen_width, self.screen_height = 800, 600
        self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))

    def tearDown(self):
        # Clean up pygame
        pygame.quit()

    def test_deadly_block_in_map(self):
        # Create a sample map
        sample_map = [
            "rddr",
            "vrrv",
            "rrrr"
        ]

        # Create a MapFactory instance
        map_factory = MapFactory("map.txt")

        # Generate the map sprites
        map_sprites = map_factory.generate_map()

        # Check if DeadlyBlock is in the map_sprites
        deadly_block_found = any(isinstance(sprite, DeadlyBlock) for sprite in map_sprites)

        # Assert that DeadlyBlock is present
        self.assertTrue(deadly_block_found, "DeadlyBlock not found in the map")


if __name__ == "__main__":
    unittest.main()
