# Import pygame and other modules
import pygame
from src.DeadlyBlock import DeadlyBlock
from src.RegularBlock import RegularBlock
from src.VictoryFlag import VictoryFlag


# Define a class for the map factory
class MapFactory:
    # Initialize the map factory
    def __init__(self, filename):
        # Load the map from a text file
        self.tile_size = 32
        self.map = []
        with open(filename, 'r') as f:
            for line in f:
                self.map.append(line.strip())

    # Generate the map sprites
    def generate_map(self):
        # Create an empty list of sprites
        map_sprites = pygame.sprite.Group()

        # Loop through the map rows and columns
        for row in range(len(self.map)):
            for col in range(len(self.map[row])):
                # Get the character from the map file
                char = self.map[row][col]
                map_element = None

                # Get the map element class from the dictionary
                if char == 'v':
                    map_element = VictoryFlag()
                elif char == 'd':
                    map_element = DeadlyBlock()
                elif char == 'r':
                    map_element = RegularBlock()
                else:
                    raise Exception("Invalide", "Invalide")

                # Check if the map element class is valid
                if map_element is not None:
                    # Set the position of the map element
                    map_element.set_position(col * self.tile_size, row * self.tile_size)

                    # Add the map element to the list of sprites
                    map_sprites.add(map_element)

        # Return the list of sprites
        return map_sprites
