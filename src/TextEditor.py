# Import pygame and other modules
import pygame


# This is a TextEditor class for a simple text editor using pygame
class TextEditor:
    # The constructor initializes the screen, font, text, cursor visibility, cursor timer, and cursor interval
    def __init__(self, screen, font_size=24):
        self.screen = screen  # The screen where the text will be displayed
        self.font = pygame.font.SysFont(None, font_size)  # The font of the text
        self.text = ""  # The text to be displayed
        self.cursor_visible = True  # The visibility of the cursor
        self.cursor_timer = 0  # The timer for the cursor
        self.cursor_interval = 500  # The interval at which the cursor blinks (in milliseconds)

    # This method handles the events like quitting the game, pressing a key
    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # If the QUIT event is triggered, quit the game
                pygame.quit()
                quit()
            elif event.type == pygame.KEYDOWN:  # If a key is pressed
                if event.key == pygame.K_BACKSPACE:  # If the key is BACKSPACE, remove the last character from the text
                    self.text = self.text[:-1]
                elif event.key == pygame.K_RETURN:  # If the key is RETURN, you can add functionality to handle this
                    # (like submitting the text)
                    pass
                else:  # If any other key is pressed, add it to the text
                    self.text += event.unicode

    # This method updates the cursor's visibility based on the cursor's timer and interval
    def update(self, dt):
        self.cursor_timer += dt  # Increase the cursor's timer
        if self.cursor_timer >= self.cursor_interval:  # If the cursor's timer has reached the interval
            self.cursor_visible = not self.cursor_visible  # Toggle the cursor's visibility
            self.cursor_timer = 0  # Reset the cursor's timer

    # This method draws the text input box, the entered text, and the cursor
    def draw(self):
        input_box_rect = pygame.Rect(20, 500, 760, 40)  # The rectangle for the text input box
        pygame.draw.rect(self.screen, (255, 255, 255), input_box_rect, 2)  # Draw the text input box

        text_surface = self.font.render(self.text, True, (255, 255, 255))  # Render the entered text
        self.screen.blit(text_surface, (input_box_rect.x + 10, input_box_rect.y + 10))  # Display the entered text

        if self.cursor_visible:  # If the cursor is visible
            cursor_x = input_box_rect.x + self.font.size(self.text)[0] + 10  # The x-coordinate of the cursor
            # Draw the cursor
            pygame.draw.line(self.screen, (255, 255, 255), (cursor_x, input_box_rect.y + 10),
                             (cursor_x, input_box_rect.y + input_box_rect.height - 10))
