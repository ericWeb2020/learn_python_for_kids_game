# Import pygame and other modules
import pygame
import sys
from Game import Game


class Menu:
    # Define screen size as constants
    SCREEN_WIDTH = 800
    SCREEN_HEIGHT = 600

    def __init__(self):
        # Initialize pygame
        pygame.init()
        # Create a screen with the defined size
        self.screen = pygame.display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        # Set the window title
        pygame.display.set_caption("Game Menu")

    def display_menu(self):
        # Create a font object
        font = pygame.font.Font(None, 36)
        # Define the color of the text
        text_color = (71, 71, 71)
        # Define the menu items
        menu_items = ["Start Game", "Quit"]
        # Lists to hold the surfaces and rectangles for the menu items
        menu_surfaces = []
        menu_rects = []

        # Loop over each menu item
        for i, item in enumerate(menu_items):
            # Render the text for the menu item
            text_surface = font.render(item, True, text_color)
            # Create a rectangle for the text
            text_rect = text_surface.get_rect(center=(self.SCREEN_WIDTH // 2, 100 + i * 50))
            # Add the surface and rectangle to their respective lists
            menu_surfaces.append(text_surface)
            menu_rects.append(text_rect)

        # Return the surfaces and rectangles
        return menu_surfaces, menu_rects

    def main(self):
        # Create a clock object
        clock = pygame.time.Clock()
        # Get the surfaces and rectangles for the menu items
        menu_surfaces, menu_rects = self.display_menu()

        # Main game loop
        while True:
            # Process events
            for event in pygame.event.get():
                # If the user closes the window, quit the game
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

                # If the user clicks the mouse
                if event.type == pygame.MOUSEBUTTONDOWN:
                    # Get the position of the mouse
                    mouse_pos = pygame.mouse.get_pos()
                    # Loop over each menu item
                    for i, rect in enumerate(menu_rects):
                        # If the mouse click was on this menu item
                        if rect.collidepoint(mouse_pos):
                            # If the first menu item is clicked, start the game
                            if i == 0:
                                game_instance = Game()
                                game_instance.run()
                            # If the second menu item is clicked, quit the game
                            elif i == 1:
                                print("Quitting the program.")
                                pygame.quit()
                                sys.exit()

            # Fill the screen with black
            self.screen.fill((0, 0, 0))
            # Draw each menu item
            for surface, rect in zip(menu_surfaces, menu_rects):
                self.screen.blit(surface, rect)
            # Update the display
            pygame.display.flip()
            # Limit the frame rate
            clock.tick(60)


if __name__ == "__main__":
    # Create a Menu object and run the main method
    menu = Menu()
    menu.main()
