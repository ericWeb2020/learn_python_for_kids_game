# Import pygame and other modules
import os
import pygame
from src.MapElement import MapElement


# Define a subclass for the flag map element
class VictoryFlag(MapElement):
    # Override the constructor
    def __init__(self):
        # Call the parent class constructor with the flag image
        MapElement.__init__(self, pygame.image.load(os.path.join('../img', 'victoryflag.png')).convert(), "vf")
