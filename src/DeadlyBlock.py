# Import pygame and other modules
import os
import pygame
from src.MapElement import MapElement


# Define a subclass for the deadly block map element
class DeadlyBlock(MapElement):
    # Override the constructor
    def __init__(self):
        # Call the parent class constructor deadly block image
        MapElement.__init__(self, pygame.image.load(os.path.join('../img', 'deadlyblock.png')).convert(), "db")
