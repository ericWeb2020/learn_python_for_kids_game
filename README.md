## Python Learning Adventure: Exploring a map

Welcome to the Python Learning Game! 

This game is designed to help kids learn coding concepts while exploring a virtual world. 🎮 Currently, only one map is available!

# Game Overview
In this game, players will navigate through a map using simple commands. 

Right now,only the commands player.advance and player.down are implemented in the game.

The goal is to reach the victory flag.

## How to Play

# Installation:
- Make sure you have Python installed on your computer. 
- I'm using ```Python 3.12```
- Clone this repository or download the game files.



# Run the Game:
- Open your terminal or command prompt.
- Navigate to the game directory.
- Run the following command:
```python menu.py```
- If you clone the repository, you can open the folder ```src``` and ```Run``` the file ```Menu.py```

- Once you start the game, an editor is there to enter the command you want the player to do. After you enter a command, the text will clear and you will be able to enter another command.
- The goal is to navigate where the road is without hitting the piked blocks

## Questions and suggestions

-  Any questions or anything related to this project, feel free to send a message on my email : eric.forget.prog@gmail.com